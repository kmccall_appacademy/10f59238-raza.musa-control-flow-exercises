# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.select {|ch| ch == ch.upcase}.join("")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_point = str.length / 2
  if str.length.odd?
    return str[mid_point]
  else
    return str[mid_point-1..mid_point]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase!
  str.chars.reduce(0) do |count, ch|
    if VOWELS.include?(ch)
      count += 1
    else
      count
    end
  end
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_string = ""
  arr.each_index do |idx|
    new_string += arr[idx]
    new_string += separator unless idx == arr.length-1
  end
  new_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.split.map do |word|
    word.chars.map.with_index do |ch, idx|
      if idx.even?
        ch.downcase
      else
        ch.upcase
      end
    end.join("")
  end.join(" ")
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map do |word|
   if word.length >= 5
     word.reverse
   else
     word
   end
  end.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |number|
    if number % 3 == 0 && number % 5 == 0
       "fizzbuzz"
    elsif number % 3 == 0
       "fizz"
    elsif number % 5 == 0
       "buzz"
    else
       number
     end
   end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  !((2...num).any? {|n| num % n == 0})
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).reduce([]) do |array, n|
    if num % n == 0
      array << n
    else
      array
    end
  end
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (1..num).select {|n| n if num % n == 0 && prime?(n)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even = []
  odd = []
  arr.each do |el|
    if el.odd?
      odd << el
    else
      even << el
    end
  end
  if odd.length == 1
    return odd[0]
  else
    return even[0]
  end
end
